@extends('layouts.default')
@section('title')
    Books list
@endsection
@section('content')
    @include('includes.booksDisplayTable')
    @include('includes.bookCreateForm')
    @include('includes.authorCreateForm')
    @include('includes.publisherCreateForm')
    @include('includes.uploadPhotoForm')
@endsection