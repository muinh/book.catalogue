<!-- Modal -->
<div class="modal fade" id="uploadPhotoModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h1>Upload photo</h1>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="" method="post" enctype="multipart/form-data" class="image-upload-form">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="upload-photo"><span class="glyphicon glyphicon-user"></span>Choose image from your filesystem</label>
                        <input type="file" class="form-control-file" id="upload-photo" name="upload-photo" required>
                    </div>
                    <button id="btn-add-photo" type="submit" class="btn btn-default btn-success btn-block"><span class="glyphicon glyphicon-off"></span>Add photo</button>
                </form>
            </div>
        </div>
    </div>
</div>