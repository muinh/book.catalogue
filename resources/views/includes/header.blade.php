<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="">Book catalog</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a id="add-book" class="nav-link">New book</a>
            </li>
            <li class="nav-item">
                <a id="add-author" class="nav-link">New author</a>
            </li>
            <li class="nav-item">
                <a id="add-publisher" class="nav-link">New publisher</a>
            </li>
        </ul>
    </div>
</nav>