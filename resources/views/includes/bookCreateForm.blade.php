<!-- Modal -->
<div class="modal fade" id="bookCreateModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h1>Add new book</h1>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="book-title"><span class="glyphicon glyphicon-user"></span>Title</label>
                    <input type="text" class="form-control" id="book-title" placeholder="Enter book title" required autofocus>
                </div>
                <div class="form-group">
                    <label for="book-author-id"><span class="glyphicon glyphicon-eye-open"></span>Author</label>
                    <select class="form-control" id="book-author-id" required>
                        <option selected disabled>Choose author</option>
                    @foreach($authors as $author)
                        <option value="{{ $author->id }}">{{ $author->name }}</option>
                    @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="book-publisher-id"><span class="glyphicon glyphicon-eye-open"></span>Publisher</label>
                    <select class="form-control" id="book-publisher-id" required>
                        <option selected disabled>Choose publisher</option>
                        @foreach($publishers as $publisher)
                            <option value="{{ $publisher->id }}">{{ $publisher->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="book-publication-date"><span class="glyphicon glyphicon-eye-open"></span>Publication date</label>
                    <input type="date" class="form-control" id="book-publication-date" placeholder="Pick publication date" required>
                </div>
                <button id="btn-add-book" type="submit" class="btn btn-default btn-success btn-block"><span class="glyphicon glyphicon-off"></span>Add new book</button>
            </div>
        </div>
    </div>
</div>