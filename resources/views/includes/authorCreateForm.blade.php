<!-- Modal -->
<div class="modal fade" id="authorCreateModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h1>Add new author</h1>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="author-name"><span class="glyphicon glyphicon-user"></span>Author name</label>
                    <input type="text" class="form-control" id="author-name" placeholder="Enter author name" autofocus required>
                </div>
                <button id="btn-add-author" type="submit" class="btn btn-default btn-success btn-block"><span class="glyphicon glyphicon-off"></span>Add new author</button>
            </div>
        </div>
    </div>
</div>