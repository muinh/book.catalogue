<!-- Modal -->
<div class="modal fade" id="publisherCreateModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h1>Add new publisher</h1>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="publisher-name"><span class="glyphicon glyphicon-user"></span>Publisher name</label>
                    <input type="text" class="form-control" id="publisher-name" placeholder="Enter publisher name" autofocus required>
                </div>
                <button id="btn-add-publisher" type="submit" class="btn btn-default btn-success btn-block"><span class="glyphicon glyphicon-off"></span>Add new publisher</button>
            </div>
        </div>
    </div>
</div>