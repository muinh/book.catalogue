<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Book title</th>
        <th scope="col">Photo</th>
        <th scope="col">Author</th>
        <th scope="col">Publisher</th>
        <th scope="col">Publication date</th>
        <th scope="col">Upload photo</th>
        <th scope="col">Delete</th>
    </tr>
    </thead>
    <tbody>
    @foreach($books as $book)
    <tr>
        <th scope="row">{{ $book['id'] }}</th>
        <td>{{ $book['title'] }}</td>
        <td>
            @if($book['photo'] != 'NULL')
            <img class="poster" src="/book.catalog/public/uploads/{{ $book['photo'] }}" alt="no-image"/>
            @else
            <img class="poster" src="#" alt="no-image"/>
            @endif
        </td>
        <td>{{ $book->author->name }}</td>
        <td>{{ $book->publisher->name }}</td>
        <td>{{ $book['publication_date'] }}</td>
        <td>
            <button class="btn btn-primary upload-photo" type="submit" data-id="{{ $book['id'] }}">
                Upload
            </button>
        </td>
        <td>
            <button class="btn btn-danger book-delete" type="submit" data-id="{{ $book['id'] }}">
                Delete
            </button>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>