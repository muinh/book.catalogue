$(document).ready(function(){


    //delete book and its image
    $('.book-delete').on('click', function() {
        var id = $(this).data('id');
        $.ajax({
            type: 'delete',
            url: 'books/' + id,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: id,
            success: function() {
                location.reload();
            }
        });
    });


    //add book
    $('#btn-add-book').on('click', function() {
        var bookTitle = $('#book-title').val();
        var bookAuthor = $('#book-author-id').val();
        var bookPublisher = $('#book-publisher-id').val();
        var bookPublicationDate = $('#book-publication-date').val();

        $.ajax({
            type: 'post',
            url: 'books',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'bookTitle': bookTitle,
                'bookAuthor': bookAuthor,
                'bookPublisher': bookPublisher,
                'bookPublicationDate': bookPublicationDate
            },
            success: function() {
                location.reload();
            }
        });
    });

    //add author
    $('#btn-add-author').on('click', function() {
        var authorName = $('#author-name').val();
        $.ajax({
            type: 'post',
            url: 'authors',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {'authorName': authorName},
            success: function(data) {
                console.log(data);
                location.reload();
            }
        });
    });


    //add publisher
    $('#btn-add-publisher').on('click', function() {
        var publisherName = $('#publisher-name').val();
        $.ajax({
            type: 'post',
            url: 'publishers',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {'publisherName': publisherName},
            success: function() {
                location.reload();
            }
        });
    });


    //add book/show modal
    $('#add-book').on('click', function(){
        $('#bookCreateModal').modal();
    });

    //add author/show modal
    $('#add-author').on('click', function(){
        $('#authorCreateModal').modal();
    });

    //add publisher/show modal
    $('#add-publisher').on('click', function(){
        $('#publisherCreateModal').modal();
    });

    //upload photo/show modal
    $('.upload-photo').on('click', function(){
        var id = $(this).data('id');
        $('#uploadPhotoModal').modal();
        $('.image-upload-form').attr('action', 'books/'+id);
    });
});

