-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 26, 2017 at 03:45 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.1.11-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `book.catalog`
--

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE `authors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Agatha Christie', '2017-11-26 10:29:31', '2017-11-26 10:29:31'),
(3, 'Antoine de Saint-Exupéry', '2017-11-26 10:32:12', '2017-11-26 10:32:12'),
(4, 'George Orwell', '2017-11-26 10:32:28', '2017-11-26 10:32:28'),
(5, 'Joanne Rowling', '2017-11-26 10:32:47', '2017-11-26 10:32:47'),
(6, 'Stephen King', '2017-11-26 10:33:09', '2017-11-26 10:33:09'),
(7, 'Theodore Dreiser', '2017-11-26 10:33:27', '2017-11-26 10:33:27');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author_id` int(11) NOT NULL,
  `publisher_id` int(11) NOT NULL,
  `publication_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `photo`, `author_id`, `publisher_id`, `publication_date`, `created_at`, `updated_at`) VALUES
(3, 'The Murder at the Vicarage', '9d104ba1f7da669c64934496085f40d3.jpeg', 1, 4, '2017-11-12', '2017-11-26 10:38:33', '2017-11-26 10:41:50'),
(4, 'The Little Prince', 'feca6a600d8a469bd5434b07753e3ee9.jpeg', 3, 3, '2017-02-14', '2017-11-26 10:38:51', '2017-11-26 10:41:59'),
(5, '1984', 'e4e45f4b94948071b41e58e3592c1daa.jpeg', 4, 1, '2017-08-17', '2017-11-26 10:39:13', '2017-11-26 10:42:08'),
(6, 'Animal Farm', '41ce1fb30a811f316922dc059e507951.jpeg', 4, 1, '2017-08-15', '2017-11-26 10:39:34', '2017-11-26 10:42:16'),
(7, 'The Lion and The Unicorn', '87f9b77a20c55cd763c84774648df6c1.jpeg', 4, 4, '2123-12-21', '2017-11-26 10:39:55', '2017-11-26 10:45:02'),
(8, 'Harry Potter and the Order of the Phoenix', '30f7d61c1b118e168ecd38136aade962.jpeg', 5, 2, '1976-02-11', '2017-11-26 10:40:17', '2017-11-26 10:42:45'),
(9, 'Harry Potter and the Philosopher\'s Stone', 'eb9247099042c7148fedeede37ef34b1.jpeg', 5, 2, '1990-12-31', '2017-11-26 10:40:37', '2017-11-26 11:18:48'),
(10, 'It', '7c4a5a0ce6c4cf29f05a19abc2e8e245.jpeg', 6, 1, '2003-12-12', '2017-11-26 10:40:58', '2017-11-26 10:43:04'),
(11, 'The Green Mile', 'e1b644fb607bd882819aa613f680491e.jpeg', 6, 1, '1989-01-01', '2017-11-26 10:41:14', '2017-11-26 10:43:13'),
(12, 'An American Tragedy', '417c21a44c1b1c198c213cd0e44cd323.jpeg', 7, 3, '1994-04-06', '2017-11-26 10:41:35', '2017-11-26 10:43:24');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(13, '2017_11_25_124425_create_authors_table', 1),
(14, '2017_11_25_124441_create_books_table', 1),
(15, '2017_11_25_124451_create_publishers_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `publishers`
--

CREATE TABLE `publishers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `publishers`
--

INSERT INTO `publishers` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Pearson', '2017-11-26 10:30:46', '2017-11-26 10:30:46'),
(2, 'Reed Elseweir', '2017-11-26 10:31:09', '2017-11-26 10:31:09'),
(3, 'Thomson-Reuters', '2017-11-26 10:31:24', '2017-11-26 10:31:24'),
(4, 'Wolters Kluwer', '2017-11-26 10:31:40', '2017-11-26 10:31:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publishers`
--
ALTER TABLE `publishers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authors`
--
ALTER TABLE `authors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `publishers`
--
ALTER TABLE `publishers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
