# Book Catalogue Task

## Laravel: Create book catalog using Laravel и MySQL:

- Create database for catalog: (Author, Book, Publisher)
- Display books list with columns (id, book name, author, publisher, publish date)
- Add delete button for book, implement via ajax
- Add forms for adding book, author, publisher, implement via ajax
- Creation forms should be created in modal windows using bootstrap
- Add main picture for the book and show in the list
- Add button to upload photo for each book 
- One book should have only one photo
- Books should be deleted together with photos