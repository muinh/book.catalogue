<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public function author()
    {
        return $this->belongsTo('App\Author');
    }

    public function publisher()
    {
        return $this->belongsTo('App\Publisher');
    }

    public function uploadImage($id, $request)
    {
        //get hashed name
        $file = $request->file('upload-photo');
        $filename = strtolower(md5(uniqid($file->getBasename()))) . '.' . $file->extension();

        $book = Book::find($id);
        self::deleteIfExists($book);
        self::savePoster($book, $file, $filename);
    }

    public static function deleteIfExists($book)
    {
        $path = 'uploads/' . $book->photo;
        if (file_exists($path)) unlink($path);
    }

    public static function savePoster($book, $file, $filename)
    {
        $file->move(public_path("/uploads"), $filename);
        $book->photo = $filename;
        $book->save();
    }
}
