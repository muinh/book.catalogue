<?php

namespace App\Http\Controllers;

use App\Author;

class AuthorsController extends Controller
{
    public function store(\Illuminate\Http\Request $request)
    {
        if (\Illuminate\Support\Facades\Request::ajax())
        {
            $author = new Author();
            $author->name = $request['authorName'];
            $author->save();
        }
    }
}
