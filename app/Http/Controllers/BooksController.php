<?php

namespace App\Http\Controllers;

use App\Author;
use App\Book;
use App\Publisher;

class BooksController extends Controller
{
    public function index()
    {
        $books = Book::all();
        $authors = Author::all();
        $publishers = Publisher::all();

        return view('books.index', compact(['books', 'authors', 'publishers']));
    }

    public function store(\Illuminate\Http\Request $request)
    {
        if (\Illuminate\Support\Facades\Request::ajax())
        {
            $book = new Book();
            $book->title = $request['bookTitle'];
            $book->author_id = $request['bookAuthor'];
            $book->publisher_id = $request['bookPublisher'];
            $book->publication_date = $request['bookPublicationDate'];
            $book->save();
        }
    }

    public function update($id, \Illuminate\Http\Request $request)
    {
        $this->validate($request, [
           'upload-photo' => 'required|image'
        ]);
        $book = new Book();
        $book->uploadImage($id, $request);

        return back();
    }

    public function delete($book)
    {
        if (\Illuminate\Support\Facades\Request::ajax()) {
            $book = Book::find($book);
            $book->delete();
            $path = 'uploads/' . $book->photo;
            if (file_exists($path)) unlink($path);
        }
    }
}
