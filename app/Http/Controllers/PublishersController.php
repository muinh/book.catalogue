<?php

namespace App\Http\Controllers;

use App\Publisher;

class PublishersController extends Controller
{
    public function store(\Illuminate\Http\Request $request)
    {
        if (\Illuminate\Support\Facades\Request::ajax())
        {
            $publisher = new Publisher();
            $publisher->name = $request['publisherName'];
            $publisher->save();
        }
    }
}
