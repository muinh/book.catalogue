<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'SiteController@home']);

Route::get('/books', ['uses' => 'BooksController@index', 'as' => 'booksIndex']);
Route::post('/books', ['uses' => 'BooksController@store']);
Route::post('/books/{book}', ['uses' => 'BooksController@update', 'as' => 'booksUpdate']);
Route::delete('/books/{book}', ['uses' => 'BooksController@delete', 'as' => 'booksDelete']);

Route::post('/publishers', ['uses' => 'PublishersController@store']);
Route::post('/authors', ['uses' => 'AuthorsController@store']);
